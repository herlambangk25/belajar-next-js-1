"use client"
import React, { useState } from 'react'
import SectionResult from './sectionResult'

export default function Page() {
  const [query, setQuery] = useState('')

  const onSearch = (e:any)=>{
    e.preventDefault()
    const inputQuery = e.target[0].value
    setQuery(inputQuery)
  }

  return (
    <div>
      <form onSubmit={onSearch} className="w-full flex space-x-3 mt-10">
        <input className='bg-zinc-100 px-3 w-5/6' placeholder='Cari User Github' />
        <button className='bg-blue-600 text-white w-1/6 py-2   rounded-md'>Cari Orang</button>
      </form>
      { query && <SectionResult query={query}/>}
      {/* {"Mencari : "+query} */}
    </div>
  )
}
