import React from 'react'
import styles from '../../styles/Dashboard.module.css'

export default function layout({children} : {children:React.ReactNode}) {
  return (
    <div className='bg-black text-white p-[20px]'>
        <p>ini layout untuk dashboard </p>
      {children}
    </div>
  )
}
