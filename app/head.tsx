
export default function Head() {
  return (
    <>
    <title>ini home</title>
    <meta content='width=device-width, initial-scale=1' name='viewport'></meta>
    <link rel="icon" href="/favicon.ico" />
    </>
  )
}
